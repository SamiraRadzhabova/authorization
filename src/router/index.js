import { createRouter, createWebHistory } from "vue-router";
import RegistrationView from "../views/RegistrationView.vue";
import AdminView from "../views/AdminView.vue";
import Page404 from "../views/Page404View.vue";

const routes = [
  {
    path: "/forma",
    name: "forma",
    component: RegistrationView,
  },
  {
    path: "/",
    name: "home",
    component: AdminView,
  },
  {
    name: "404",
    path: "/:pathMatch(.*)*",
    component: Page404,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((to, from, next) => {
  const token = localStorage.getItem("access_token");

  if (to.name !== "forma" && !token) {
    next({ name: "forma" });
  } else if (to.name === "home" && !token) {
    next({ name: "forma" });
  } else {
    next();
  }
});

export default router;

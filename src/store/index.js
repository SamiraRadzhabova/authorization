import { createStore } from "vuex";
export default createStore({
  strict: process.env.NODE_ENV !== "production",
  namespaced: true,
  state: {
    listsubadmins: [],
    permissions: [],
    userPermission: [],
  },
  mutations: {
    // Отримання всіх субадмінів
    getListSubadmins(state, listsubadmins) {
      state.listsubadmins = listsubadmins;
    },

    // Усі дозволи які є (для мультиселекту)
    getPermission(state, permissions) {
      state.permissions = permissions;
    },

    // Дані за кнопкою "Дозвіли" для певного користувача
    getUserPermission(state, userPermission) {
      state.userPermission = userPermission;
    },
  },
});
